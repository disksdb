/**
* This interface describes all posible operation with discs.
* @author Andrey Parhomenko
* @version 0.1
* @date 05.04.2010
*/ 

package ua.edu.sumdu.lab3.model;

import ua.edu.sumdu.lab3.model.exceptions.*;
import java.util.Date;
import java.util.List;

public interface Operable {
    
    public void addDisc(Disc disc) throws AddDataException;
    
    public void addGenre(String genre) throws AddDataException;
    
    public void addCountry(String country) throws AddDataException;
    
    public void addLabel(String label) throws AddDataException;
    
    public Disc getDiscById(int id) throws GetDataException;
    
    public List getDiscsByGenre(String genre) throws GetDataException;
    
    public List getDiscsByAuthor(String author) throws GetDataException;
    
    public List getDiscsByLabel(String label) throws GetDataException;
    
    public List getDiscsByDate(Date date) throws GetDataException;
    
    public List getDiscsByTitle(String title) throws GetDataException;
    
    public List getTitles() throws GetDataException;
    
    public List getAuthors() throws GetDataException;
    
    public List getGenres() throws GetDataException;
    
    public List getLabels() throws GetDataException;
    
    public List getDates() throws GetDataException;
    
    public void editDisc(int id) throws EditDataException;
}

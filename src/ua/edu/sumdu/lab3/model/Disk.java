import java.util.Date;

class Disk {
    
    /* uniq identifier of the album */
    private int id;
    /* title of the album */
    private String title;
    /* author of the album*/
    private String author;
    /* album's genre */
    private String genre;
    /* label of the album */
    private String label;
    /* release date */
    private Date date;

    public Disk(int id, String title, String author, 
                String genre, String label, Date date) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.label = label;
        this.date = date;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }
    
    public void setGenre(String genre) {
        this.genre = genre;
    }
    
    public void setLable(String label) {
        this.label = label;
    }
    
    public void serDate(Dat date) {
        this.date = date;
    }
}

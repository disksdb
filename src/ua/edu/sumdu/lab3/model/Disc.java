/**
 * @author Andrey Parhomenko
 * @version 0.1
 * @date 05.04.2010
 */

package ua.edu.sumdu.lab3.model;

import java.util.Date;

class Disc {
    
    /* uniq identifier of the album */
    private int id;
    /* title of the album */
    private String title;
    /* author of the album*/
    private String author;
    /* album's genre */
    private String genre;
    /* label of the album */
    private String label;
    /* release date */
    private Date date;

    /**
    * New instanse of the class Disk.
    * 
    * @param id identifier
    * @param title title of the album
    * @param author author of the album
    * @param genre genre of the album
    * @param label label of the album
    * @param date date of release
    */
    public Disc(int id, String title, String author, 
                String genre, String label, Date date) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.label = label;
        this.date = date;
    }
    
    /**
    * Sets id of the album.
    * @param id id of the album.
    */
    public void setId(int id) {
        this.id = id;
    }
    
    /** 
    * Sets title of the album.
    * @param title title of the album. 
    */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
    * Sets author of the album.
    * @param author author of the album. 
    */ 
    public void setAuthor(String author) {
        this.author = author;
    }
    
    /**
    * Sets genre of the album
    * @param genre genre of the album
    */  
    public void setGenre(String genre) {
        this.genre = genre;
    }
    
    /**
    * Sets label of the album.
    * @param label label of the album.  
    */ 
    public void setLabel(String label) {
        this.label = label;
    }
    
    /**
     * Sets date of the album.
     * @param date date of the album.
     */ 
    public void setDate(Date date) {
        this.date = date;
    }
    
    /**
    * Returns id of the album.
    * @return id of the album.
    */ 
    public int getId() {
        return this.id;
    }
    
    /**
    * Returns title of the album.
    * @return title of the album.
    */ 
    public String getTitle() {
        return this.title;
    }
    
    /**
    * Returns author of the album.
    * @return author of the album.
    */  
    public String getAuthor() {
        return this.author;
    }
    
    /**
    * Returns genre of the album.
    * @return genre of the album.
    */ 
    public String getGenre() {
        return this.genre;
    }
    
    /**
    * Returns label of the album.
    * @return label of the album.
    */ 
    public String getLabel() {
        return this.label;
    }
    
    /**
    * Returns date of the album.
    * @return date of the album.
    */ 
    public Date getDate() {
        return this.date;
    }
}
